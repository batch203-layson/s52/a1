import { Container } from "react-bootstrap";

import Home from "./pages/Home";

import Courses from "./pages/Courses";

import Register from "./pages/Register";


import Login from "./pages/Login";

import './App.css';

import AppNavBar from "./components/AppNavBar";


/* 
all other components/pages will be contained in our main (parent) component: <App />
*/

/* 
We use React fragment (<>...</>) to render adjacent 
*/
function App() {
  return (
    <>
      <AppNavBar />,
      <Container>
       {/*  <Home />
        <Courses /> */}
       {/*  <Register /> */}
       <Login/>
      </Container>
    </>
  );
}

export default App;
