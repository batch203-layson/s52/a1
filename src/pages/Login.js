import { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";


export default function Login() {

    const [electronicMail, setElectronicMail] = useState('');
    const [passwordTwo, setPasswordTwo] = useState('');

    const [isActive, setIsActive] = useState(false);
    
    const prefEmail = "admin@mail.com";
    const prefPass = "admin";

    useEffect(() => {
        // Enable the button if:
        // All the fields are populated 
        // both passwords match 

        if (
            (
                electronicMail !== ''
                &&
                passwordTwo !== ''
            )
            &&
            (
                electronicMail === electronicMail
                &&
                passwordTwo === passwordTwo
            )
        ) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }

    }, [electronicMail, passwordTwo])






    function loginUser(e) {
        // Prevents page loading/ redirection via form submission
        e.preventDefault();

        // clear input fields
        setElectronicMail('');
        setPasswordTwo('');

        // Notify user for registration
        if
        (
            electronicMail === prefEmail
            &&
            passwordTwo === prefPass
        )
        {
            alert(`Login successful!`);
        }
        else
        {
            alert(`Login failed!`);
        }
        
    }

    return(
        <>
            <h1 className="my-5 text-center">LOGIN</h1>

            <Form
                onSubmit={e => loginUser(e)}
            >


                <Form.Group
                    className="mb-3"
                    controlId="emailAddress"
                >
                    <Form.Label>Email address</Form.Label>

                    <Form.Control

                        type="email"
                        placeholder="Enter email"
                        value={electronicMail}
                        onChange={e => setElectronicMail(e.target.value)}
                        required

                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>


                <Form.Group
                    className="mb-3"
                    controlId="setPasswordTwo"
                >
                    <Form.Label>Verify Password</Form.Label>

                    <Form.Control
                        type="password"
                        placeholder="Verify Password"
                        value={passwordTwo}
                        onChange={e => setPasswordTwo(e.target.value)}
                        required
                    />
                </Form.Group>

                {/* 
                Conditional Rendering
                submit button will be active based on the isActive state
                 */}
                {
                    isActive
                        ?
                        <Button
                            variant="primary"
                            type="submit"
                            id="submitBtn"
                        >
                            Submit
                        </Button>
                        :
                        <Button
                            variant="danger"
                            type="submit"
                            id="submitBtn"
                            disabled
                        >
                            Submit
                        </Button>
                }


            </Form>
        </>
        

    )

}