import CourseCard from "../components/CourseCard";

import coursesData from "../data/coursesData";

export default function Courses() {

    // To confirm if we have import our mock database.
    console.log(coursesData);

    console.log(coursesData[0]);
    // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions.

    // Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesDara array using courseProp.
    // Multiple components created through the map method maust have a unique key that will help ReactJS identify which components/elements have been changed, added, or removed.
    const courses = coursesData.map(course => {
        return (
            <CourseCard key={course.id} courseProp={course} />
        );
    });


    return (
        <>
            <h1>Courses</h1>
            {courses}
        </>
    )
}
