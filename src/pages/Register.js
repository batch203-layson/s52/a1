import { useEffect, useState } from "react";
import{Form, Button} from "react-bootstrap";

export default function Register() {

    /* 
    create state hooks to store the values of the input fields 
    */
    const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // create a state to determine whether the submit button is enabled or not

    const [isActive, setIsActive] = useState(false);

    /* 
    Two way binding

    is done so that we can assure that we can save the input into our states as we type into the input elements. This is so we don't have to save it just before submit.

    e.target = current element where the event happened

    e.target.value = 

    */

    /* 
    Check if the values are successfuly binding 
    */
    console.log(fName);
    console.log(lName);
    console.log(email);
    console.log(mobileNo);
    console.log(password1);
    console.log(password2);

    useEffect(()=>{
        // Enable the button if:
        // All the fields are populated 
        // both passwords match 
        
        if(
            (
                fName!==''
                &&
                lName!==''
                &&
                email!==''
                &&
                mobileNo!==''
                &&
                password1!==''
                &&
                password2!== ''
            )
                &&
            (
                password1 === password2
            )
        )
        {
            setIsActive(true);
        }
        else
        {
            setIsActive(false);
        }

    }, [fName, lName, email, mobileNo, password1, password2])

    
    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page loading/ redirection via form submission
        e.preventDefault();

        // clear input fields
        setFName('');
        setLName('');
        setEmail('');
        setMobileNo('');
        setPassword1('');
        setPassword2('');

        // Notify user for registration
        alert(`${fName} Thanks for registering!`);
    }
    
    return (
     <>
            <h1 className="my-5 text-center">Register</h1>

            <Form
                onSubmit={e=>registerUser(e)}
            >

                <Form.Group 
                    className="mb-3" 
                    controlId="firstname"
                >
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                    
                    type="text" 
                    placeholder="Enter First Name"
                    value={fName}
                    onChange={e => setFName(e.target.value)}
                    required 

                    />
                </Form.Group>

                <Form.Group
                    className="mb-3"
                    controlId="lastname"
                >
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control

                        type="text"
                        placeholder="Enter Last Name"
                        value={lName}
                        onChange={e => setLName(e.target.value)}
                        required

                    />
                </Form.Group>

           
                
                <Form.Group 
                    className="mb-3" 
                    controlId="emailAddress"
                >
                    <Form.Label>Email address</Form.Label>

                    <Form.Control 
                    
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required

                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>


                <Form.Group
                    className="mb-3"
                    controlId="mobileNo"
                >
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control

                        type="number"
                        placeholder="09xxxxxxxxx"
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required

                    />
                </Form.Group>

                <Form.Group 
                    className="mb-3" 
                    controlId="password1"
                >
                    <Form.Label>Password</Form.Label>

                    <Form.Control 
                        type="password" 
                        placeholder="Enter Password" 
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required    
                    />
                </Form.Group>

                <Form.Group
                    className="mb-3"
                    controlId="password2"
                >
                    <Form.Label>Verify Password</Form.Label>

                    <Form.Control
                        type="password"
                        placeholder="Verify Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

                {/* 
                Conditional Rendering
                submit button will be active based on the isActive state
                 */}
                 { 
                 isActive
                    ?
                        <Button
                            variant="primary"
                            type="submit"
                            id="submitBtn"
                        >
                            Submit
                        </Button>
                    :
                        <Button
                            variant="danger"
                            type="submit"
                            id="submitBtn"
                            disabled
                        >
                            Submit
                        </Button>
                  }
               

            </Form>

     </>
    )
}