// [SECTION] Creating React Application:
	// Syntax:
		npx create - react - app < project - name >

    // Delete unecesary files from the app
    // application > src
    App.test.js
index.css
logo.svg
reportWebVitals.js

// Remove the importation of the "index.css" and "reportWebVitals" files from the "index.js" file. Also remove the code using the reportWebVitals function.
// Application > src > index.js

// Remove the importation of the "logo.svg" file and most of the codes found inside the "App" component to remove any errors.
// Application > src > App.js

/*
    The syntax used in Reactjs is JSX.

        - JSX - Javascript + XML, It is an extension of Javscript that let's us create objects which will then be compiled and added as HTML elements.

        - With JSX, we are able to create HTML elements using JS.

        - With JSX, we are able to create JS objects that will then be compiled and added as HTML elements.

*/

/*
    ReactJS Component

        - This are reusable parts of our react application.
        - They are independent UI parts of our app.
        - Components are functions that return react elements.
        - Components naming Convention: PascalCase
            - Capitalized letter for all words of the function name AND file name associated with it.
*/

// React Bootstrap Components
/*
    Syntax:
        import { moduleName/s } from "file path"

*/

/*
    React.StrictMode is a built-in react component which is used to highlight potential problems in our code and in fact allows for more information about errors in our code.
*/

/*
    React import pattern:
        -imports from built-in react modules.
        -imports from downloaded packages
        -imports from user defined components
*/

/*
Mini Activity

    1. Create a "CourseCard" component showing a particular course with the name, description and price inside a React-Bootstrap Card.
        - Import the "Card" and "Button" in the react-bootstrap.
        - The "Card.Body" should contain the following:
            - The course name should be in the "Card.Title".
            - The "description" and "price" label should be in the "Card.Subtitle".
            - The value for "description" and "price" should be in the "Card.Text".
            - Add a "Button" with "primary" color for the Enroll.
    2. Create a "Courses.js" page and render the "CourseCard" component inside of it.
    3. Render also the "Courses" page in the parent component to mount/display it in our browser.
    4. Take a screenshot of your browser and send it in the batch hangouts

*/

/*
    Props
        - is a shorthadn for "property" since components are considered as object in ReactJS
        - is a way to pass data from a parent component to a child component.
        - it is synonymous to function parameters.
        - it is used like an HTML attribute added to the child component.

*/

/*

    States
    - States are a way to store information within a component. This information can then be updated within the component. 
    - States are used to keep track of information related to individual components.

    Hooks 
    - Special/react-defined methods and functions that allow us to do certain tasks in our components.
    - Use the state hook for this component to be able to store its state
	
*/

/* 
wait for push nalang sa resource
*/


    // syntax
        // useEffect(function, [dependencyArray])

    // No dependency passed
        // If the useEffect() does not have a dependency array, it will run on initial render and whenever a state is set by its function
  /*   useEffect(()=>{
        console.log("We run on initial render or on every changes with our components");
  });
   
 */

//An empty array
//if the userEffect() has dependency array but it is empty, it will only run on the initial render
/*   useEffect(()=>{
    console.log("Will only run on initial render.");
  }, []); */

// With dependency array (props or state values)
//   if the useEffect() has a dependency array and there is state or data in it, the useEffect will run whenever that state is updated.


/* 
useEffect(()=>{
    console.log("Will run on initial render and every change on the dependency value.");
}, [seats, count]); */